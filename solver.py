import numpy as np
asudoku=[[1,4,5,3,2,7,6,9,8],
        [8,3,9,6,5,4,1,2,7],
        [6,7,2,9,1,8,5,4,3],
        [4,9,6,1,8,5,3,7,2],
        [2,1,8,4,7,3,9,5,6],
        [7,5,3,2,9,6,4,8,1],
        [3,6,7,5,4,2,8,1,9],
        [9,8,4,7,3,1,2,3,5],
        [5,2,1,8,3,0,0,0,0]]
sudoku=[[5,3,0,0,7,0,0,0,0],
        [6,0,0,1,9,5,0,0,0],
        [0,9,8,0,0,0,0,6,0],
        [8,0,0,0,6,0,0,0,3],
        [4,0,0,8,0,3,0,0,1],
        [7,0,0,0,2,0,0,0,6],
        [0,6,0,0,0,0,2,8,0],
        [0,0,0,4,1,9,0,0,5],
        [0,0,0,0,8,0,0,7,9]]
bsudoku=[[0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0]]
counter = 0

# def get_neighbours(x,y):
#     neighbours=np.empty([1,2],dtype=int)
#     for row in range(9):
#         for col in range(9):
#             if row == x or col == y or (np.floor(row/3) == np.floor(x) and
#                     np.floor(col/3) == np.floor(y)):
#                 neighbours = np.append(neighbours,[[row,col]],axis=0)
#     return neighbours[1:]
# 
# def possible(x,y,newnum):
#     global sudoku
#     neighbours = get_neighbours(x,y)
#     for neighbour in neighbours:
#         if sudoku[neighbour[0]][neighbour[1]] == newnum:
#             return False
#             break
#     return True

def possible(y,x,n):
  global sudoku
  # n is the number we want to fill in

  # 1st 
  # check if n already existed in vertical (y) axis
  # if exists, return False (not possible)
  for i in range(9):
    if sudoku[y][i] == n:
      return False

  # 2nd
  # check horizontal (x) axis
  for i in range(9):
    if sudoku[i][x] == n:
      return False

  # 3rd
  # check the 3x3 local grid
  x0 = (x//3)*3 
  y0 = (y//3)*3
  for i in range(3):
    for j in range(3):
      if sudoku[y0+i][x0+j] == n:
         return False

  # return true if pass all 3 checks.
  return True

def is_solved():
    global sudoku
    for row in range(9):
        for col in range(9):
            if sudoku[row][col] == 0:
                return False
    return True

def solve():
    global sudoku
    for row in range(9):
        for col in range(9):
            if sudoku[row][col] == 0:
                for cand in range(1,10):
                    if possible(row,col,cand):
                        sudoku[row][col]=cand
                        solve()

                        sudoku[row][col]=0
                return
    print(np.matrix(sudoku))

def main():
    # print(np.matrix(sudoku))
    solve()
    # print(ossible([8,8],4))

if __name__=="__main__":
    main()
